(function() {
  'use strict';

  angular
    .module('reportuser', [])
    .controller('reportuserController', loadFunction);

  loadFunction.$inject = ['$scope', 'structureService', '$location', '$http'];

  function loadFunction($scope, structureService, $location, $http) {
    // Register upper level modules
    structureService.registerModule($location, $scope, 'reportuser');
    // --- Start reportuserController content ---

    $scope.statusError = "";

    init();

    function init() {
      checkLogin();

    }

    function checkLogin() {
      var allGood = false;

      if (localStorage.getItem('dstoken')) {
        var req = {
          method: 'GET',
          url: 'http://34.219.60.121:3000/signinup/v1/helloteca/check',
          headers: {
            'x-web-key': "Development",
            'X-DS-TOKEN': localStorage.getItem('dstoken'),
            'accept': "*/*"
          }
        }

        $http(req).then(function(response) {
          $scope.showModule = true;
          $scope.showError = false;
          getData();

        }, function(error) {
          catchError("Ha fallado la petición", error)
        });
      } else {
        $scope.showModule = false;
        $scope.showError = true
        $scope.statusError = "Ha caducado la sesión.";
        catchError("No te has logado o el token se ha borrado");
      }

    }

    function getData() {
      var req = {
        method: 'GET',
        url: 'http://34.219.60.121:3000/helloteca/v1/mobile/report',
        headers: {
          'x-web-key': "Development",
          'X-DS-TOKEN': localStorage.getItem('dstoken'),
          'accept': "*/*"
        }
      }

      $http(req).then(function(response) {
        var report = response.data.data.Report;

        adjustresponse(report);

      }, function(error) {
        catchError("Ha fallado la petición", error);
        $scope.showModule = false;
        $scope.showError = true
        $scope.statusError = "Ha caducado la sesión.";
      });
    }

    function adjustresponse(report) {
      angular.forEach(report, function(value, key) {
        var tempReport = report;
        tempReport[key].date1 = (report[key].date) ? report[key].date.split(" ")[0] : "";
        tempReport[key].tyme = (report[key].date) ? report[key].date.split(" ")[1] : "";

        var actualColor = tempReport[key].color;
        var colors = {
          "yellow": '#FED731',
          "blue": '#22CCD5',
          "red": '#EA4B57',
          "orange": '#FAAF3F'
        }
        tempReport[key].color = colors[actualColor];

        var icons = {
          "status": "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:sketch=\"http://www.bohemiancoding.com/sketch/ns\" viewBox=\"0 0 30 30\" version=\"1.1\" x=\"0px\" y=\"0px\"><title>action_001-edit-pen-pencil-write-compose</title><desc>Created with Sketch.</desc><g stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\" sketch:type=\"MSPage\"><g sketch:type=\"MSArtboardGroup\" transform=\"translate(-45.000000, -45.000000)\" fill=\"#ffffff\"><g sketch:type=\"MSLayerGroup\" transform=\"translate(50.000000, 47.000000)\"><path d=\"M16.0049107,5 L1.99508929,5 C0.893231902,5 0,5.8926228 0,6.99508929 L0,21.0049107 C0,22.1067681 0.905082651,23 2.00285136,23 L13.4753903,23 L11.4753903,21 L2,21 L2,7 L16,7 L16,17.0393283 L18,19.0393283 L18,6.99658373 C18,5.89390099 17.1073772,5 16.0049107,5 Z\" sketch:type=\"MSShapeGroup\" transform=\"translate(9.000000, 14.000000) scale(1, -1) translate(-9.000000, -14.000000) \"></path><path d=\"M16.4980807,1.6276279 L12.4980807,1.6276279 L12.4980807,13.6276279 L16.4980807,13.6276279 L16.4980807,1.6276279 Z M16.4341177,14.8723149 L14.9341177,17.5453489 C14.7430887,17.8857669 14.2530727,17.8857669 14.0620437,17.5453489 L12.5620437,14.8723149 C12.5201067,14.7975809 12.4980807,14.7133239 12.4980807,14.6276279 L16.4980807,14.6276279 C16.4980807,14.7133239 16.4760547,14.7975809 16.4341177,14.8723149 Z M16.4980807,-0.872372098 C16.4980807,-1.1485141 16.2742227,-1.3723721 15.9980807,-1.3723721 L12.9980807,-1.3723721 C12.7219387,-1.3723721 12.4980807,-1.1485141 12.4980807,-0.872372098 L12.4980807,0.627627902 L16.4980807,0.627627902 L16.4980807,-0.872372098 Z\" sketch:type=\"MSShapeGroup\" transform=\"translate(14.498081, 8.214145) rotate(-315.000000) translate(-14.498081, -8.214145) \"></path></g></g></g></svg>",
          "dni": "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" x=\"0px\" y=\"0px\" viewBox=\"26 -26 100 100\" style=\"enable-background:new 26 -26 100 100;\" xml:space=\"preserve\" fill=\"#ffffff\"><style type=\"text/css\">.st0{fill-rule:evenodd;clip-rule:evenodd;}</style><path class=\"st0\" d=\"M114.3,1.1L63.8,52.3c-1.7,1.8-4.6,1.8-6.3,0L36.7,31c-1.7-1.8-1.7-4.6,0-6.4c1.7-1.8,4.6-1.8,6.3,0l17.7,18.1  l47.4-48c1.7-1.8,4.6-1.8,6.3,0C116.1-3.5,116.1-0.7,114.3,1.1z\"></path></svg>",
          "state": "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 100 100\" enable-background=\"new 0 0 100 100\" xml:space=\"preserve\" fill=\"#ffffff\"><rect fill=\"none\" width=\"100\" height=\"100\"></rect><g><path d=\"M50,17.656c0,0-7.489-0.881-6.608,7.269l4.186,43.616H50h2.422l4.187-43.616C57.489,16.775,50,17.656,50,17.656z\"></path><circle cx=\"50\" cy=\"76.617\" r=\"5.754\"></circle></g></svg>"
        }

        // tempReport[key].icon =  "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 100 100\" enable-background=\"new 0 0 100 100\" xml:space=\"preserve\" fill=\"#ffffff\"><rect fill=\"none\" width=\"100\" height=\"100\"></rect><g><path d=\"M50,17.656c0,0-7.489-0.881-6.608,7.269l4.186,43.616H50h2.422l4.187-43.616C57.489,16.775,50,17.656,50,17.656z\"></path><circle cx=\"50\" cy=\"76.617\" r=\"5.754\"></circle></g></svg>";
        var actualicon = tempReport[key].icon;
        if (tempReport[key].icon) {
          if (actualColor != "orange") {
            tempReport[key].icon = icons[actualicon];
          } else {
            tempReport[key].icon = "<svg xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" version=\"1.1\" x=\"0px\" y=\"0px\" viewBox=\"0 0 100 100\" enable-background=\"new 0 0 100 100\" xml:space=\"preserve\"><path fill=\"#ffffff\" d=\"M92.5,44.3h-5.1c-2.4-6.7-8.8-11.6-16.3-11.6c-7,0-13.1,4.2-15.8,10.3c-1.5-1.1-3.3-1.8-5.3-1.8  c-2,0-3.8,0.7-5.3,1.8c-2.7-6-8.7-10.3-15.8-10.3c-7.5,0-14,4.9-16.3,11.6H7.5v5h4.2c0,0.2,0,0.4,0,0.7c0,9.5,7.7,17.3,17.3,17.3  S46.2,59.5,46.2,50c0-2.1,1.7-3.8,3.8-3.8s3.8,1.7,3.8,3.8c0,9.5,7.7,17.3,17.3,17.3S88.4,59.5,88.4,50c0-0.2,0-0.4,0-0.7h4.2V44.3z   M28.9,62.3c-6.8,0-12.3-5.5-12.3-12.3s5.5-12.3,12.3-12.3S41.2,43.2,41.2,50S35.7,62.3,28.9,62.3z M71.1,62.3  c-6.8,0-12.3-5.5-12.3-12.3s5.5-12.3,12.3-12.3S83.4,43.2,83.4,50S77.9,62.3,71.1,62.3z\"></path></svg>";
          }
        }

        var defoutButtonValue = "Continuar";
        if (!tempReport[key].button) {
          tempReport[key].button = defoutButtonValue;
        }

        $scope.report = tempReport;
      });

    }

    function catchError(error) {
      console.error("error", error);
    }
    // --- End reportuserController content ---
  }
}());
